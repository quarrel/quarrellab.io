/**
 * Copyright (c) Jake Russo.
 *
 * This source code is licensed under the MIT license found in the
 * LICENSE file in the root directory of this source tree.
 */
import React from "react";
import mermaid from "mermaid";

mermaid.initialize({startOnLoad: true});

class Mermaid extends React.Component {
  constructor(props, theme) {
    // console.debug("Mermaid component constructing...");
    super(props);
    this.state = {
      theme: theme ?? 'base',
      chart: props.children
    }
  }
  insertSVG(svgCode) {
    this.innerHTML = svgCode;
  }
  componentDidMount() {
    mermaid.contentLoaded();
  }
  render() {
    return <div className="mermaid" id={this.props.id}>
    {`%%{init: {'theme':'${this.state.theme}'}}%%\n` +
     this.props.children}
    </div>
  }
}

class ContractGraph extends Mermaid {
  constructor(props) {
    // console.debug("ContractGraph component constructing...");
    const theme = 'dark';
    super(props, theme);
  }
}

export default ContractGraph;