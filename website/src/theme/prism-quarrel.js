Prism.languages.quarrel = {
  'comment': [
    {
      pattern: /^###[\s\S]*\n###/,
      greedy: true
    },
    {
      pattern: /\#\[(?:(?!\]\#)[^\\\r\n])*\]\#/,
      // lookbehind: true,
      greedy: false
    },
    /^#.+/,
    {
      pattern: /([^\]])#(?!\[|\]).*/,
      lookbehind: true,
      // greedy: true
    },
  ],
  'function': [
    {
      pattern: /\b([a-zA-Z_][a-zA-Z0-9_]*)\p{Zs}+(?="|\d|\w|\(|\[|\{)/u,
      alias: 'selector',
    },
    {
      pattern: /\b([a-zA-Z_][a-zA-Z0-9_]*)(?=\(|\!)/,
      alias: 'selector',
    },
  ],
  'element': [
    {
      pattern: /\b[a-zA-Z_][a-zA-Z0-9_]*(?:\.(?!\.)|:)/,
      alias: 'function',
    },
    {
      pattern: /([^.])\.[a-zA-Z_][a-zA-Z0-9_]*\b/,
      lookbehind: true,
      alias: 'function',
    },
    {
      pattern: /([^:]):[a-zA-Z_][a-zA-Z0-9_]*\b/,
      lookbehind: true,
      alias: 'function',
    },
  ],
  'string': {
		pattern: /"(?:\\(?:\r\n|[\s\S])|(?!")[^\\\r\n])*"/,
		greedy: true
	},
  'number': [
    {
      pattern: /(16\^\^)[\da-fA-F][\da-fA-F_]*(?:\.[\da-fA-F_]+)?\b/,
      lookbehind: true,
    },
    /\b\d[\d_]*(?:\.[\d_]+)?\b/,
  ],
  'datetime': {
    pattern: /`(?:\\(?:\r\n|[\s\S])|(?!`)[^\\\r\n])*`/,
		greedy: true,
    alias: 'number'
  },
  'boolean': /\b(?:true|false|yes|no)\b/,
  'empty': {
    pattern: /(\W)\(\)\!?/,
    lookbehind: true,
    alias: 'class-name'
  },
  'keyword': [
    {
      pattern: /<\+|\+>|\|?\->|<-\|?|<\|{1,3}|>>?-|-<?<|<\$|\$>|\*(?!\s)/,
      alias: 'class-name',
    },
    {
      pattern: /(\w|\d|\s|\)|\}|\])(?:\/\\|\\\/|\=\=\>|\<\=\>)(?=\w|\d|\s|\(|\{|\[)/,
      lookbehind: true,
      alias: 'class-name',
    },
    {
      pattern: /((?:\w|\d|\)|\])\[)(?:\+|\@|\\|\/|\!|\?|\#)(?=\])/,
      lookbehind: true,
      alias: 'class-name',
    },
    {
      pattern: /(\w|\d|\s|\)|\}|\])[=~*]\>(?=\w|\d|\s|\(|\{|\[)/,
      lookbehind: true,
      alias: 'class-name',
    },
    {
      pattern: /[@~?!]@/,
      alias: 'class-name',
    },
    {
      pattern: /[$~?!]\$/,
      alias: 'class-name',
    },
    {
      pattern: /=?~(?!@)/,
      alias: 'class-name',
    },
  ],
  'class-name': [
    /\b[a-zA-Z_][a-zA-Z0-9_]*(?:\?|\^)/,
    /\b[a-zA-Z_][a-zA-Z0-9_]*(?=\{|\s*[\?\^]\=)/,
  ],
  'variable': {
    pattern: /'(?:\\(?:\r\n|[\s\S])|(?!')[^\\\r\n])*'/,
    greedy: true,
  },
  'builtin': {
    pattern: /\|(?:[-=]\|)?|\/=\//,
  },
  'operator': [
    {
      pattern: /&{1,3}|\|{1,3}>?|\^\^\^|!!!|==|<>|\>=|<=|>{3}|>{2}(?!-)|<<<?|(?<!\>)\+{1,3}(?!\>)|(?<!\>)\-{1,3}(?!\>)|\/{1,3}(?!=)|%{1,2}|\*(?=\s)|\*{2,3}|\.{2}/,
      greedy: true
    },
    {
      // >: >! :< :! :: !> :> <! <:
      pattern: />[!:]|:[<!:]|[!:]\>|\<[!:]/,
      // alias: 'class-name',
    },
    {
      pattern: /(\w|\d|\s|\)|\}|\])(?:\.{3})(?=\w|\d|\s|\(|\{|\[|,|\]|\})/,
      lookbehind: true,
      alias: 'class-name',
    },
  ]
}

Prism.languages.qvr = Prism.languages.quarrel;