import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Transcendental',
    Svg: require('../../static/img/undraw_Powerful_re_frhr.svg').default,
    description: (
      <>
        Highly expressive and complex, Quarrel is like nothing else. A carefully planned collection of aesthetically-pleasing visual aids that transport your understanding to higher planes. The distinction between what's syntax and what's named data breeds a mental model that promotes understanding and expressiveness in equal parts.
      </>
    ),
  },
  {
    title: 'Multi-paradigm',
    Svg: require('../../static/img/undraw_Work_chat_re_qes4.svg').default,
    description: (
      <>
        Multiple dispatch, first-class routines, first-class patterns, and dataflow primitives provide functional and actor programming primitives. On the other hand, you can dive deep into structured programming with patterns and transformative contracts. Quarrel gives you baked-in tools that support many different approachs to a problem.
      </>
    ),
  },
  {
    title: 'Visceral',
    Svg: require('../../static/img/undraw_ideation_re_8i2h.svg').default,
    description: (
      <>
        Hand-curated "form follows function" syntax that is completely alien to the traditions of programming syntax while still feeling intuitive enough that one can <a href="https://en.wikipedia.org/wiki/Grok#In_computer_programmer_culture">grok</a> it without a Computer Science PhD. Quarrel changes the domain of programming comprehension from an internal monologue to something akin to flow diagrams.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
