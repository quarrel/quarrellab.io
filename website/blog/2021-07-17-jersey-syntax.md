---
slug: jersey-syntax
title: Jersey-Style Syntax Refresh
author: Jake Russo
author_title: Quarrel Designer
author_url: https://gitlab.com/madcapjake
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/113840/avatar.png?width=400
tags: [pldesign, syntax, invocation, sequences, routines]
---

At long last, a clean design for all data representations in Quarrel has been reached. The design caused some major changes and has effectively winnowed down Quarrel to a place where there is enough abstraction to be comfortable in the available syntax that you can make with the characters on most people's keyboards. 
<!--truncate-->
Quarrel now has aggregate data organized into a trichotomy of usage:

1. Storage (Tables) `[...]`
2. Procedure (Sequences) `(...)`
3. Structure (Patterns/Records) `{...}`/`(...)`

This threefold approach has allowed Quarrel to reduce the total symbolic footprint which should improve reading comprehension. That being said, the table semantics  do take some getting used to if you've never been exposed to Lua. This part of the refresh was an attempt to graft the Table semantics of Lua into the hole left by Lists and Maps of the old syntax.

## Translating Into Quarrel

Here is where it all began a week or so ago. Translating tutorial examples[\[1\]][1][\[2\]][2] from other languages into Quarrel syntax is a major way that I have built up the syntax/semantics of Quarrel in my head. 

```quarrel
Suit ?= Spades{} | Hearts{} | Diamonds{} | Clubs{}

Option<Type> ?= None{} | Some{Type? value}
# no need to give the value a container name either
Option<Type> ?= None{} | Some{Type}

expt .= None? _, n => expt(Some 2; n)
expt += Some? b, 0 => 1
expt += Some? b, n =>
    n %% 2 @@ expt(Some b*b; n/2) ~@ b * expt(Some b; n-1)
```

```quarrel
Tree<A> ?= Empty{}
         | Node{Tree<A>, A, Tree<A>}


height .= Empty? _ => 0
       += Node? {l, _, r} => 1 + (height(l) +++ height(r))
```

The `{...}` inside of the parameter pattern indicates a destructuring

```quarrel
Expr ?= Number{} | Plus{Expr & Expr} | Times{Expr & Expr}

eval .= {Expr\Number? n} => n
     += {Plus}


# tables ala Lua
fruit .= [strawberries. 10, tomatoes: 5]
```

## Demo Track

Here's some of my earlier thoughts, kept here for historic reasons:

:::note Earlier Thoughts

After a few days brewing on some revolutionary changes in Quarrel, I think I've landed on a model that once-and-for-all arrives at an intuitive model of sequences and patterns. There has been some collateral damage, partly warranted; partly just felt natural with the new conventions. 

Firstly, both maps and lists have been unified into one literal circumfix `[...]`. To be honest, I was hesitant to utilize `<...>` in such a common place since it doesn't have the greatest support amongst text editors for treating it as a delimiter (mostly due to `>` and `<` operators causing ambiguity). This means that lookups are using the same `[...]` postcircumfix and that feels intuitive and familiar to many programmers coming from other languages. This also means that the method operators will be shared. Lastly, I don't know where this leaves Sets. Either, they will remain using `<...>` or they will be relegated to a named contract (which makes some sense). Still haven't made up my mind either way.

I think we will largely follow Lua semantics and just combine both into one thing calld tables.

:::

Another big change is that the invocation syntax has come back from the `[...]` postcircumfix to the `(...)` postcircumfix (which is the same syntax as many programming languages). This will both increase familiarity and has a specific conception in the new aggregate model. When you give a container name and supply a postcircumfix `(...)` operator to it, you are turning a sequence from a procedural one into a structural one. These structural sequences form the basis of both Contract-defined Records and Routines.

Next on the task list is to finish cleaning up any remaining vestages of the old syntax. Then, on to more content on patterns/records. After that, converting Sets into a syntactic quirk of Tables. From there...TBD, there's much yet to write and I want to follow where my enjoyement is to keep the creative energy flowing.

Thanks for reading!

## Definitions

:::note Parameter Pattern

A pattern supplied to a routine transform operation which is utilized in container invocation by being applied to the **structural sequence** that was provided in the invocation (the arguments). The application of which, results in a contextual set of parameters with names as given in its declaration.

:::

:::note Contractual Pattern

A pattern that is used to either validate data or generate a context (the internal structure of, and types/names within the container). When a contract is invoked the contractual pattern is either used to validate or generate a data structure based on the structural sequence provided.

:::

:::note Structural Sequence

The arguments provided to either a **Contract** or a **Routine** which is then passed through the parameter pattern to estabalish either the routine's context or the contract instance's internal structure.

:::

:::note Procedural Sequence

The arguments provided to an untagged sequence which is left unevaluated until either the container that holds this sequence is invoked or a parent expression evaluates certain operators (such as arithmetic).

:::

[1]:https://www.cs.cmu.edu/~rwh/introsml/core/datatypes.htm
[2]:http://lua-users.org/wiki/TablesTutorial

\[1\]:https://www.cs.cmu.edu/~rwh/introsml/core/datatypes.htm

\[2\]:http://lua-users.org/wiki/TablesTutorial