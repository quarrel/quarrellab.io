---
slug: a-woke-invoke
title: A Woke Invoke
author: Jake Russo
author_title: Quarrel Designer
author_url: https://gitlab.com/madcapjake
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/113840/avatar.png?width=400
tags: [pldesign, syntax, invocation, sequences, routines]
---

Using this blog entry as a jotpad for an idea I am exploring. With the newly-minted, unified invocation syntax `[]`, I now have two postcircumfix operators that are essentially untied to any semantics. I had a thought wherein the syntax of `someContainer(...)` could be best spent strengthening the semantic meaning of the parentheses in the first place: as a sequence expression.
<!--truncate-->
By that, I mean that whatever is inside the parentheses should always be a series of expressions so as to not add additional cognitive dissonance to the programmer's mind. Tangientially speaking, couldn't the very nature of languages maintaining familiar syntax be a dialectical response to our mind's desire to avoid all cognitive dissonance whenever possible. I find that highly likely to be a part of the story.

So, where was I? Yes, the `someContainer(...)` should at least semantically require that whatever is contained within `...` is a series of expressions. The question is, what does the `someContainer` name mean, semantically? One possible interpretation would be as a shorthand for a sequence/routine with a single argument that is a sequence/routine.

```quarrel
someContainer(_ + 1) # is equiv to
someContainer[(_ + 1)]
```

This is actually quite a nice syntax sugaring as it avoids noisy brackets/parentheses combos. Some other options that have come to mind:

- Named sequence shorthand, this would allow for recursive calls within the sequence
- A shorthand for `someList -> (...)`, essentially applying a sequence to each list element
- Possibly as some syntactic feature within concurrency?

---

On a separate note, I'm also reviewing the I/O-type builtins that are provided to see if there's any room for additional stdlib features to still fit in the syntax.

```quarrel
|-|   # STDIN/STDOUT
*|-|  # STDERR
|=|   # Event Queue?
/=/   # Child Process Event Queue?
||-|| # ??
||=|| # ??
```

There's still a few useful combinations that even have corresponding ligatures in FiraCode. The challenge is making it seem logical and intuitive instead of just an arbitrary selecting of symbols.

That's it for today. After writing this blog post, I also got through a few of those special transform operators and tweaked a paragraph in the Sequences page. A good day's work... 'til next time!

