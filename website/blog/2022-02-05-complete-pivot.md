---
slug: complete-syntactic-pivot
title: Complete Syntactic Pivot
author: Jake Russo
author_title: Quarrel Designer
author_url: https://gitlab.com/madcapjake
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/113840/avatar.png?width=400
tags: [pldesign, syntax, redesign]
---

My posts on this blog have dried up in recent months (about a half of a year) due to a couple rabbit holes that I've gone down. Playgrounds for syntaxes that have never been. I toyed around with a language formed entirely with HTML-like tags (a fairly-unexplored space in PL design) and then had some nostalgia for transpiler languages of yore. Around that time, I had a new idea taking hold...

<!--truncate-->

What if there was a CoffeeScript but capable of being compiled down to executable binaries? A hyper-elegant rapid-prototyping language that compiles into speedy machine code. With that one seed planted, I started preparing to write a compiler for CoffeeScript.

As usual, the devil reared his head and said "what if you changed this?" and "what if we completely shift to that?", plus other questions of a mutative, innovative, opinionative nature. So I may have started with CoffeeScript, what I finally landed on, it honestly felt like Quarrel.

For a time, it was going by the name of KafaScript in an ode to its origins. I kept having to update the tagline from "CoffeeScript remagined" to "inspired by..." to "a new language with nods to..." and now, I am barely sure there is much DNA left.

However, all the differences aside, I still wanted--at the core--a language that *writes* like CoffeeScript. engages your code-elegance mind and gives you the tools to write in an almost pseudo-code-like fashion. So, I needed to keep some pieces of the Node/JavaScript inner-workings, so things like the core object protocol, standard library, and module system should feel just like you remember.

In fact, keeping this core has freed my mental gymnastics that were needed for a type system. JavaScript/CoffeeScript and their ilk, mostly punt type-checking to the developer in usage guards and unit tests. This language would do the same. In the interest of focusing in on an API that matches this model, I plan on implementing a standard library to be very closely similar to Node and Deno standard libraries (picking and choosing the pieces that I find the most agreeable to this language's styles). There will be some modifications made to make significant use of the `with` operator.

## Quarrel 4.0

All that said, I am planning on retiring the current Quarrel design documents and replacing them with a Quarrel 4.0 that is a rebranded KafaScript. This will be better for my mental health to only be working on one language. Plus, I have limited time to develop this so being able to focus my energy in one place, means I can maximize progress.

Finally, I am working to prepare myself for this change and trying to question if each design decision should be moved into the archive or retro-fitted for KafaScript's forms. From a vision perspective, this change is a complete pivot from what I had originally intended Quarrel for (removing English from programming). Mind you, I still think this original vision is beautiful and righteous but I don't think it's a path that will be laden with supporters (or even many brave explorers). The new QKS syntax (as I'll be calling this new syntax) will be *closely* tied with the English language; as many pieces (owing to it's CoffeeScript origin) are words themselves and intended to be used in "sentences" that function like built-in documentation.

That being said, the likelihood that my amateur programming mind could fully implement something as monumental and innovative are slim and might even be a drag due to the slow and difficult progression (I bit off more than I could chew). The QKS syntax allows for quick modeling based off of prior art in the domain (mainly CoffeeScript, Python and JavaScript for design choices utliized). My current challenge is reading and understanding  `grammar.js` files in other Tree-sitter parsers. This effort is much more fruitful than going in hog-wild with never-before-seen syntax.

So, I've mostly landed on making this change happen. I'd love to get it transferred over this weekend but we will see how much energy I have. 

Cheers!