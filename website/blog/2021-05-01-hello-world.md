---
slug: hello-world
title: Hello World
author: Jake Russo
author_title: Quarrel Designer
author_url: https://gitlab.com/madcapjake
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/113840/avatar.png?width=400
tags: [site, docusaurus, intro]
---

I've decided it's time to start getting Quarrel out there in the world. It may not be loved by everyone due to its many alien syntax and new semantics. However, I think it's got something special that the world needs to see. Only time will tell. 

In the mean time, I will continue toiling away at this mess of syntax. There are always little shifts that occur which I will document here and what reasoning went into them. Currently, the language is mostly hashed-out however as I expand the documentation, I am finding things that can be tweaked to be more intuitive or to simplify/reduce down what is extraneous and thus provide more operators that are available for future growth of the language. One thing is for sure, when you restrict your design to only just the most commonly-found-on-keyboards symbols, you run out of material real quick!

I will also delve into details about the vision of Quarrel and why I set out on this journey. Feel free to stick around for PL design diatribes and more! Thanks for checking out Quarrel!
