---
slug: docs-galore
title: Docs Galore
author: Jake Russo
author_title: Quarrel Designer
author_url: https://gitlab.com/madcapjake
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/113840/avatar.png?width=400
tags: [site, pldesign, docusaurus]
---

My first mission is to bring the basic `syntax.qvr` file found in the `qlc` repo and transcribe it into Markdown whilst also adding additional details. Here in, lies some details of that journey including a Docusaurus migration and a new syntax highlighting plugin.

<!--truncate-->

Firstly, I had this site up a few months back using Docusaurus v1. Easy to setup on gitlab pages and centered around documentation meant it was a perfect fit for Quarrel docs. After setting it all up, I took a break from programming and returned later only to the `syntax.qvr` file.

Finally, I came back to Docusaurus and wanted to get a custom Quarrel syntax highlighter into it. So, I checked out the docs and it was a fully supported, one-command insert that gives you some code to start adding your own language to the highlighter (Prism.js). When I ran the command, I got an error that `swizzle` wasn't a known command. 

That's when I went on a journey of trial-and-error only to finally arrive at: Docusaurus is v2 now and you have to migrate. Well, since I had barely done any writing on the site, I downloaded the main site page (which does have some interesting copy that I will eventually add back into the new site) and proceeded to purge the whole directory.

After some fiddling with a new Docusaurus v2 install, I finally got it up and running and it's looking great! Added the logo and some of the smaller bits of copy back to the home page. Viola!

Now to add my custom Quarrel highlighter grammer. Unfortunately Prism.js doesn't have any fancy parsing engine. Each highlighted piece is one regular expression. No matter, I whipped up a few basic ones and grabbed some of the larger single expressions from the VSCode plugin. 

So now, I have written some docs, I have a basic syntax highlighter and a great Docusaurus v2 site. The sky is the limit, there's so much I want to add.

* More tutorials covering all of the language features
* A few language-X to Quarrel pages to help transition other language users over to Quarrel
* Some more pages covering vision, contributions, roadmap
* A page describing optimal programming environments for Quarrel (ligatures, syntax highlighting, code ditor plugins, etc.)

My approach to language development is to get at least through that first bullet point before diving too far into developing the compiler. I want to have a great reference to use as a specification for how to write the lexer/parser.

Additionally, this approach allows the Quarrel syntax to be more fungible in the design phase. Which means I can make changes without too much worry for any downstream damage. I want Quarrel to feel hand-crafted and intuitive and this means never being satisfied until it starts to all feel "second nature".
