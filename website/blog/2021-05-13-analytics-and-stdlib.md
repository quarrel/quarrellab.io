---
slug: analytics-and-stdlib
title: Analytics & Standard Library
author: Jake Russo
author_title: Quarrel Designer
author_url: https://gitlab.com/madcapjake
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/113840/avatar.png?width=400
tags: [site, pldesign, docusaurus]
---

I've taken some time over these last couple of days to continue to add more details here and also read some other language websites for inspiration into the best approach to organizing this site. Reading other language sites has also provided some help in filling out the features that Quarrel provides in its standard library. Keeping an ever vigilant eye for things that could be integrated directly into the language proper.

<!--truncate-->

Firstly, I was curious if there were any non-google analytics tools out there and after a quick sweep through some search results I landed on a nice little [list](https://mentalpivot.com/ethical-web-analytics-alternatives-google/) of some ethical alternatives. It was here that I looked through several and found a wide variety of price points. I was considering throwing a little cash this way but realized one was friendly to non-commercial ventures such as this! [Goatcounter](https://www.goatcounter.com/) is the name. Quite evocative! I had figured I'd need to create a plugin and quickly hacked something up from the google-analytics plugin. Upon searching NPM to see if my name was already taken, I discovered that someone had already built this! Additionally, they had worked out a much better approach than my initial prototype. I've opened the [analytics](https://quarrel-institute.goatcounter.com/) up to anyone in the spirit of being open as my only intention with anayltics is to help improve the site and help understand what programmers looking at fringe languages are looking for!

After getting that built, I added some more details to the standard library. After recently coming to the conclusion that Quarrel can support Sets by just making them key-value-less maps (and how that has a bit of intuitiveness to it), I added even some method operators for these newcomers to Quarrel. I had originally put together a big list of method ops to support the different methods usually available in language standard libraries. Then I realized that many of these were binary operations that would be better served by integrating them with the math operators. Since Quarrel seeks to put symbolism front-and-center, it does require that many operations are supported by the math operators. Great care must go into providing the most intuitive arrangement and there may be a time that certain operations become named to improve clarity (I predict the language could make a turn back towards less obsessive symbolism depending on how it grows and what domains it excels--or just becomes popular--in).

Thanks for reading and stay tuned for more developmental musings as I navigate the development of a non-academic language and try to self teach programming language theory. I've recently been reading about concurrency primitives to see if there's any design lessons for Quarrel in where other languages have tread.
