---
slug: hundred-doors
title: Hundred Doors Exercise
author: Jake Russo
author_title: Quarrel Designer
author_url: https://gitlab.com/madcapjake
author_image_url: https://gitlab.com/uploads/-/system/user/avatar/113840/avatar.png?width=400
tags: [pldesign, syntax, rosetta-code]
---

As another exercise, I am going to be translating the [100 doors](http://rosettacode.org/wiki/100_doors) rosetta code example written in [Standard ML](https://www.smlnj.org/).
<!--truncate-->
```sml
datatype Door = Closed | Opened
 
fun toggle Closed = Opened
  | toggle Opened = Closed
 
fun pass (steps, doors) = List.mapi (fn (k, door) => if (k+1) mod steps = 0 then toggle door else door) doors
 
(* [1..n] *)
fun runs n = List.tabulate (n, fn k => k+1)
 
fun run n =
	let
		val initialdoors = List.tabulate (n, fn _ => Closed)
		val runs = runs n
	in 
		foldl pass initialdoors runs
	end
 
fun opened_doors n = List.mapPartiali (fn (k, Closed) => NONE | (k, Opened) => SOME (k+1)) (run n)
```

```quarrel
Door ?= Closed{} | Opened{}

toggle    .= {Closed{}} => Opened()
toggle[+] .= {Opened{}} => Closed()

skip .= {Door? d} => d

pass .= {steps, doors} => (
    [1::doors[@]] +> doors -> {k, door} =>
        k %% steps @@ toggle door ~@ skip door
)

run .= {n} => (
    initialDoors .= [1::n] -> Closed()
    runs .= [1::n]
    runs >- pass -> initialDoors
)

openedDoors .= {n} => (r .= run n; [1::r[@]] +> r) $> {k, Opened{}} => k

```