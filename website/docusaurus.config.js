/** @type {import('@docusaurus/types').DocusaurusConfig} */
module.exports = {
  title: 'Quarrel',
  tagline: 'The Symbolic Programming Language',
  url: 'https://quarrel.institute',
  baseUrl: '/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'quarrel', // Usually your GitHub org/user name.
  projectName: 'quarrel.gitlab.io', // Usually your repo name.
  plugins: ['docusaurus-plugin-goatcounter'],
  themeConfig: {
    goatcounter: {
      code: 'quarrel-institute',
    },
    prism: {
      theme: require('prism-react-renderer/themes/nightOwl'),
    },
    navbar: {
      title: 'Quarrel',
      logo: {
        alt: 'Quarrel Logo',
        src: 'img/quarrel_logo.png',
      },
      items: [
        // Install
        // Learning
        // Vision
        // Thanks
        // Development
        // Guides
        // Docs
        // Community
        // Blog
        {
          type: 'doc',
          docId: 'quarrel/introduction',
          position: 'left',
          label: 'What is Quarrel?',
        },
        {
          type: 'doc',
          docId: 'getting-started/installation',
          position: 'left',
          label: 'Installation',
        },
        {
          type: 'doc',
          docId: 'guides/fundamentals',
          position: 'left',
          label: 'Documentation',
        },
        {to: '/blog', label: 'Blog', position: 'left'},
        {
          href: 'https://gitlab.com/quarrel',
          label: 'Source Code & Issue Tracker',
          position: 'right',
        },
      ],
    },
    footer: {
      style: 'dark',
      links: [
        {
          title: 'Documentation',
          items: [
            {
              label: 'Introduction',
              to: '/docs/quarrel/introduction',
            },
          ],
        },
        {
          title: 'Community',
          items: [
            {
              label: 'Stack Overflow',
              href: 'https://stackoverflow.com/questions/tagged/quarrel',
            },
            {
              label: 'Discord',
              href: 'https://discordapp.com/invite/docusaurus',
            },
            {
              label: 'Twitter',
              href: 'https://twitter.com/docusaurus',
            },
          ],
        },
        {
          title: 'More',
          items: [
            {
              label: 'Blog',
              to: '/blog',
            },
            {
              label: 'GitLab',
              href: 'https://gitlab.com/quarrel',
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Quarrel Institute, Inc. Built with Docusaurus.`,
    },
  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          editUrl:
            'https://gitlab.com/quarrel/quarrel.gitlab.io/-/edit/master/website/',
        },
        blog: {
          showReadingTime: true,
          editUrl:
            'https://gitlab.com/quarrel/quarrel.gitlab.io/-/edit/master/website/blog/',
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
