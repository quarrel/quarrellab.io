---
sidebar_position: 1
---

# Installation

Let's discover **Quarrel in less than 5 minutes**.

## Installation

Use QVM (Quarrel Version Manager).

```shell
qvm install truffle-quarrel latest
# or
qvm install llvm-quarrel latest
qvm select truffle latest
```

## Run a Quarrel script

Then create a new `*.qvr` file.

```shell
touch hello-world.qvr
echo "\"Hello World\" -> |-|" > hello-world.qvr
qvm run hello-world.qvr
Hello World
```

## Nothing is Working

That's because none of this has been written yet! All of the above will likely change but here is where additional shell instructions would be provided.
