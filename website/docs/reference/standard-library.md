---
sidebar_position: 1
---

# Standard Library

The following routines, operators and contracts are provided as part of the standard library.

## Primitive Contracts

Quarrel is a gradually-typed language which means there is no need to specify what type of data you are using but you can add contracts on a per-data basis where needed. Quarrel provides several builtin primitive contracts for all of the builtin literals and some auxiliary ones as well.

### Numeric

When you use a number literal, Quarrel will store it as an integer if no decimal point, a decimal if there is one, and one of the radix-types if the radix operator `^^` is used. You can also use `^` to indicate scientifix notation (followed by the degree) for any of the numeric literals.

```quarrel
Number? 10
Integer? 100
Decimal? 10.5
Binary? 2^^10110
Octal? 8^^12312
Hexadecimal? 16^^1A2F31
```

### Text
Texts are special lists of graphemes (or characters). The literal form uses the `"` operator (the `'` operator is for symbol literals).

```quarrel
Text? "Wow"
Grapheme? "c" # also written with double quotes but restricted to one grapheme
```

### Date/Time
Quarrel uniquely contains a literal type for dates and times. They are a very common type of data and to avoid additional word-based namespaces and routes, Quarrel has opted for something more builtin to the syntax. Dates can be identified by having a base specified as `[8601]^^` followed by a special date/time formatted literal. This format is known as [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) and Quarrel supports nearly all of the different representations within the ISO format.

```quarrel
Date? [8601]^^2020-10-04
DateTime? [8601]^^2020.10.04T15:42:58+00:00
Time? [8601]^^00:12:34.567Z
```
:::tip Custom Literals
Quarrel plans to support other formats using `[x]^^` format in the future. You can also add your own by adding a name and a regular expression. The only limitation is no spaces are allowed. The code fails on the use of `\s` and ignores actual spaces.
```quarrel
^^(+) "FunkyNum", </ [\dzZ]+ />
Literal{FunkyNum}? [FunkyNum]^^12z123Z12z492
```

:::


### Truth Value
In Quarrel, there are the usual `true` and `false` literals, but also `yes` and `no` which are equivalent to their respective counterparts. There are several primitve contracts to support using truth values.

```quarrel
Truth? 10 >> 5 # any yes/true, no/false result
Yes? test 10
True? 10 >> 5
No? fail 5
False? 1 + 1 == 11
```

### Aggregate
Quarrel provides Sequences, Lists, Maps, Sets, and Patterns for aggregate data types. Sequences use `( )` operators to both create and invoke. Lists use `[ ]` operators to create and index. Maps and Sets both use `< >` but with maps, you provide a key-value pair. Patterns use `{ }` for creation, parametric arguments, and for creating subcontracts (literal named patterns within a pattern container accessible with `->` or `\` operators).

```quarrel
Sequence? 1, 2
List? [3, 4]
Map? <a. 1, b. 2>
Set? <1, 2, 3>
Pattern? {Number? n}
Aggegrate? [2, 3] # any sequence, list, map, set or pattern
```

### Auxiliary
These primitive contracts will help you check  metadata around containers.

```quarrel
Container? x # any non-literal
Datum? d # any non-pattern, non-routine, non-aggregate
Data? d # plural datum
Actor? a # any non-() element
Empty? e # checks if value is ()
Reference? r # is this value a reference to another datum?
```

## Primitive Method Operators
All primitives come with method operators that use the main syntactic grouping operator (`( )`, `[ ]`, `{ }`, or `< >`) and an infix operator (e.g., `[+]`) to represent builtin methods that are included within the standard language.

### Sequence

| Method Operator | Action                                                |
|-----------------|-------------------------------------------------------|
|      `(+)`      | Add a new routine to the container                    |
|      `(:)`      | Clone the container of sequences                      |
|      `(_:)`     | Cloned list of all parameter patterns                 |
|      `(?)`      | Check for a matching parameter pattern                |
|      `(-)`      | Change a routine by looking up it's parameter pattern |
|      `(@)`      | Count of how many routines are contained              |

### List

| Method Operator | Action                                                              |
|-----------------|---------------------------------------------------------------------|
|      `[x]`      | Get the `x`th element                                        |
|      `[+]`      | Push a new list item onto the end of the list                       |
|      `[@]`      | Return the size of the list                                         |
|      `[!]`      | Delete a list item (at index provided)                              |
|      `[<:]`     | First element of the list                                           |
|      `[:>]`     | Last element of the list                                            |
|     `[<:x]`     | First `x` element(s) of the list                                    |
|     `[x:>]`     | Last `x` element(s) of the list                                     |
|      `[<!]`     | Take the first element, removing it from the original list          |
|      `[!>]`     | Take the last element, removing it from the original list           |
|     `[<!x]`     | Take the first `x` element(s), removing them from the original list |
|     `[x!>]`     | Take the last `x` element(s), removing them from the original list  |

### Map

| Method Operator | Action                                     |
|-----------------|--------------------------------------------|
|      `<x>`      | Get the value at the `x` key               |
|      `<?>`      | Check if map has a key                     |
|      `<@>`      | Return the size of the map (how many keys) |
|      `<!>`      | Delete a key and its value                 |
|      `<_:>`     | Get a list of the map's keys               |
|      `<:_>`     | Get a list of map's values                 |

### Set

| Method Operator | Action                               |
|-----------------|--------------------------------------|
|      `<x>`      | Check if the set contains this value |
|      `<+>`      | Add a value to a set                 |
|      `<@>`      | Return the size of the set           |
|      `<!>`      | Delete a value in a set              |

### Pattern

| Method Operator                                                  | Action                                                                                                          |
|------------------------------------------------------------------|-----------------------------------------------------------------------------------------------------------------|
| `{C1,C2,...}` | Positional contract/pattern parameters that are used inside patterns that have `\1`, `\2`, etc. template parameters.                        |
|            `(p1,p2,...)`           | Positional parameters that are provided in the order that the pattern indicates to create custom structured aggregate data. |
|                         `{@}`                        | The size of the custom structured data that can be represented by this pattern.                                             |