---
sidebar_position: 7
---
import ContractGraph from '@theme/ContractGraph';

# Sets

A Set is an orderless aggregate that contains unique elements only. The keys in a Quarrel table fit these requirements. Therefore, Sets are really just tables where the keys are the only important piece.

## How to...


### create a set

```quarrel
toppings .= <["pepperoni", "sausage", "artichokes", "black olives"]>
```

### add a value to a set

```quarrel
toppings["hot sauce"] = yes
```

### check if set contains a value

```quarrel
toppings["artichokes"] #= yes
```

### remove a value from a set

```quarrel
toppings[!] "sausage"
```

### convert to a table

```quarrel
toppings[#]
```

## Operators

Binary and unary operations that act specifically on Sets.

### `-`

Resolves to the set difference or all the elements of the `x` Set that aren't in the `y` Set.

```quarrel
{Set? x, Set? y} => Set? (...)
```

#### Examples

```quarrel
<[1,2,3]> - <[1,2]> #= <3>
<[1,2,3]> - <[1,2,3]> #= <[()!]>
<["a", "b", "c"]> - <["c"]> #= <["a", "b"]>
```

### `|`

Resolves to the set union or all the elements of the `x` Set that aren't in the `y` Set.

```quarrel
{Set? x, Set? y} => Set? (...)
```

#### Examples

```quarrel
<[1,2,3]> | <[1,2]> #= <[1,2,3]>
<[1,2,3]> | <[1,2,3,4]> #= <[1,2,3,4]>
<["a", "b", "c"]> | <["c", "d", "e"]> #= <["a", "b", "c", "d", "e"]>
```

### `&`

Resolves to the set intersection or all the elements of the `x` and `y` Sets that are in both sets.

```quarrel
{Set? x, Set? y} => Set? (...)
```

#### Examples

```quarrel
<[1,2,3]> & <[1,2]> #= <[1,2]>
<[1,2,3]> & <[2,3,4]> #= <[2,3]>
<["a", "b", "c"]> & <["c", "d", "e"]> #= <["c"]>
```

### `==`

Resolves to `true` if the left Set is an exact copy of the right Set

```quarrel
{Set? x, Set? y} => Truth? (...)
```

#### Examples

```quarrel
<[1,2,3]> == <[1,2]> #= false
<[1,2,3]> == <[1,2,3]> #= true
```

### `<>`

Resolves to `false` if the left Set is an exact copy of the right Set

```quarrel
{Set? x, Set? y} => Truth? (...)
```

#### Examples

```quarrel
<[1,2,3]> <> <[1,2]> #= true
<[1,2,3]> <> <[1,2,3]> #= false
```

### `>>`

Resolves to `true` if the left Set is a strict superset of the right Set

```quarrel
{Set? x, Set? y} => Truth? (...)
```

#### Examples

```quarrel
<[1,2,3]> >> <[1,2]> #= true
<[3,4,5]> >> <[3,7]> #= false
```

### `<<`

Resolves to `true` if the left Set is a strict subset of the right Set

```quarrel
{Set? x, Set? y} => Truth? (...)
```

### `>=`

Resolves to `true` if the left Set is a superset of or equal to the right Set

```quarrel
{Set? x, Set? y} => Truth? (...)
```

#### Examples

```quarrel
<[1,2,3]> >= <[1,2]> #= true
<[3,4,5]> >= <[3,4,5]> #= true
<[3,4,5]> >= <[3,4,5,7]> #= false
```

### `<=`

Resolves to `true` if the left Set is a subset of or equal to the right Set

```quarrel
{Set? x, Set? y} => Truth? (...)
```

#### Examples

```quarrel
<[1,2,3]> <= <[1,2,3]]> #= true
<[3,4,5]> <= <[3,7,4,5]> #= true
<[3,4,5,2]> <= <[3,7,4,5]> #= false
```

### `***`

Picks a number of random elements from the set. Anything integer greater than 1 will return a list of picked elements instead of just one.

```quarrel
{Set{\1}? x, 1}                   => \1? (...)
{1, Set? y}                       => \1? (...)
{Set{\1}? x, Integer? y @ _ >> 1} => List{\1}? (...)
{Integer? x @ _ >> 1, Set{\1}? y} => List{\1}? (...)
```

#### Examples

```quarrel
<[1,2,3]> *** 2 #= [3,1]
s .= <["apples","carrots","potatoes"]>
s *** 1 #= "carrots"
s *** 2 #= ["potatoes", "carrots"]
s *** 1 #= "potatoes"
s *** s[@] #= ["carrots", "apples", "potatoes"]
```



## Method Operators

Method operations that are performed on Set containers.

### `[x]`

Check if the set contains this value

```quarrel
{Set? self, Datum? x} => Truth? (...)
```

#### Examples

```quarrel
colors .= <["Green", "Yellow", "Red"]>
colors["Blue"] #= false
colors["Red"] #= true
colors[3] #= false
```

### `[+]`

Add a value to a set  

```quarrel
{Set? self, Datum? x} => self
```

#### Examples

```quarrel
tens .= <[10, 20, 30]>
tens[+] 40 #= <[10, 20, 30, 40]>
```

### `[@]` 

Return the size of the set 

```quarrel
{Set? self} => Integer? (...)
```

#### Examples

```quarrel
fives .= <[5, 10, 15]>
fives[@] == 3 #= true
```

### `[!]`

Delete a value in a set 

```quarrel
{Set? self, Datum? x} => self
```

#### Examples

```quarrel
ones .= <[1,2,3,4.5,5]>
ones[!][4.5] #= <[1,2,3,5]>
ones[4.5] #= false
```

## Contract Graph

<ContractGraph id="set-graph">
graph LR;
  Set --> Aggregate;
  Aggregate --> Data;
  Data --> ac([Actor]);
  Set --> an([Any]);
</ContractGraph>