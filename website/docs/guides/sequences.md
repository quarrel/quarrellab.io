---
sidebar_position: 6
---
import ContractGraph from '@theme/ContractGraph';

# Sequences

A sequence is an ordered aggregate that contains a series of expressions that are evaluated upon invocation. The `=>` operator can be used to attach a lefthand parameter pattern to a righthand sequence expression. This is known as a **Routine**. When invoking a routine, any arguments provided are passed through the pattern which will either fail due to not fulfilling the pattern's features or will gain contextual containers that are then in-scope for the sequence's body of expressions.

## How to...


### create a sequence

```quarrel
seq .= (1,2,3)
```

### create a sequence with positional parameters

```quarrel
addTen .= (\1 + 10)
```

### invoke a sequence

```quarrel
result .= seq! #= 3
n .= (2 + 4) + 3
```
The `(2 + 4)` sequence is eagerly invoked by the `+` operator.

### invoke with arguments

```quarrel
timesTen .= (\1 * 10)
timesTen(5) #= 50 (parentheses to invoke with arguments)
timesTen 10 #= 100 (syntax sugar allows parentheses-less invocations)
```

If a parentheses-less invocation becomes ambiguous, you can use a `;` to terminate.

### convert to a routine

```quarrel
scientificNotation .= (\1 * 10 ** \2)
e .= {num, sig} => scientificNotation
e 2.34, 7 #= 23,400,000
```

### declare a routine

You can declare a routine the same way you convert, by supplying a lefthand pattern and a righthand sequence to the `=>` operator.

```quarrel
foo .= {x} => (x + x * 10)
foo 10 #= 200

bar .= x, y => x + (y * 10)
bar[2, 4] #= 42
bar 4, 2  #= 24
```

## Operators

Binary and unary operations that act specifically on Sequences.

### `->`

This operator will do several difference types of transformations depending on the receiver. In the case of being received by a sequence, the supplying sequence is provided as arguments to the receiving sequence. In the case of being received by a pattern, the supplying sequence is tested against the pattern to determine if it matches.

```quarrel
{Sequence? x, Sequence? y} => Any? y(...x)
{Sequence? x, Pattern? y} => Truth? (y? x)
{Sequence? x, Contract? y} => Truth? (y? x)
```

#### Examples

```quarrel
i .= 1, result .= [], i >> 100 -> i .= result[+] .= i * 3 #= [1, 3, 9, 18, 54, 162]
result .= [1], result[@] >> 100 -> result[+] .= result[@] * 3 #= [1, 3, 9, 18, 54, 162]


seq .= (1, 2, 3)
seq -> |-| #= 3
```
The `|-|` builtin container is for input and output and in this case, it prints whatever value is provided to it to the STDOUT.

### `<-`

Take the receiving values as arguments to an invocation of the sequence.

```quarrel
{Sequence? x, Sequence? y} => Any? x(...y)
{Routine? x, Sequence? y} => Any? x(...y)
{Sequence? x, Any? y} => Any? x(y)
{Routine? x, Any? y} => Any? x(y)
```

#### Examples

```quarrel
g .= (\1 + \2 + \3)
g <- (1, 2, 3) #= 6
```

### `+`

Invokes any sequence provided as an argument to the `+` operator.

```quarrel
{Sequence? x, Any? y} => Any? x! + y
{Any? x, Sequence? y} => Any? x + y!
```

#### Examples

```quarrel
(1,2,3) + 5 #= 8
```

### `==`

Invokes any sequence provided as an argument to the `==` operator.

```quarrel
{Sequence? x, Any? y} => Any? x! == y
{Any? x, Sequence? y} => Any? x == y!
```

#### Examples

```quarrel
(1 + 1) == 11 #= false
(1 & 1) == 11 #= true
```

### `<>`

Invokes any sequence provided as an argument to the `<>` operator.

```quarrel
{Sequence? x, Any? y} => Any? x! <> y
{Any? x, Sequence? y} => Any? x <> y!
```

#### Examples

```quarrel
(1 + 1) <> 11 #= false
```

### `>>`

Invokes any sequence provided as an argument to the `>>` operator.

```quarrel
{Sequence? x, Any? y} => Any? x! >> y
{Any? x, Sequence? y} => Any? x >> y!
```

#### Examples

```quarrel
(1 + 1) >> 11 #= false
```

### `<<`

Invokes any sequence provided as an argument to the `<<` operator.

```quarrel
{Sequence? x, Any? y} => Any? x! << y
{Any? x, Sequence? y} => Any? x << y!
```

#### Examples

```quarrel
(1 + 1) << 11 #= false
```

### `>=`

Invokes any sequence provided as an argument to the `>=` operator.

```quarrel
{Sequence? x, Any? y} => Any? x! >= y
{Any? x, Sequence? y} => Any? x >= y!
```

#### Examples

```quarrel
(1 + 1) >= 11 #= false
```

### `<=`

Invokes any sequence provided as an argument to the `<=` operator.

```quarrel
{Sequence? x, Any? y} => Any? x! <= y
{Any? x, Sequence? y} => Any? x <= y!
```

#### Examples

```quarrel
(1 + 1) <= 11 #= false
```

### `***`

Invoke the sequence as many times as indicated by the other operand.

```quarrel
{Sequence{\1}? x, 1}                   => \1? x!
{1, Sequence{\1}? y}                   => \1? x!
{Sequence{\1}? x, Integer? n @ _ >> 1} => Table{\1}? [x! #[1]#, x! #[2]#, ..., x! #[n]#]
{Integer? n @ _ >> 1, Sequence{\1}? y} => Table{\1}? [y! #[1]#, y! #[2]#, ..., y! #[n]#]
```

#### Examples

```quarrel
n .= 5; (n .= n + 5) *** 3 #= [10, 15, 20]
```


## Method Operators

Method operations that are performed on Sequence containers. Since they are accessing the internal table of each container, they use the table postcircumfix delimiters `[...]`.

### `[+]`

Add a new routine to the container

```quarrel
{Container{Sequence}? self, Sequence? x} => self
```

#### Examples

```quarrel
convert .= {Number? n, Truth? percent} => (percent @@ n ~@ n * 100)
convert[+] .= {Number? n} => n
```

### `[:]`

Clone the container of sequences   

```quarrel
{Container{Sequence}? self} => (new := self)
```

#### Examples

```quarrel
fs .= {1} => "one"
fs[+] .= {2} => "two"
apply .= {Sequence? f, Number? arg, Number? times} => (
  count := times
  i := arg
  [(i .= i + 1, count .= count - 1) >> 0 -> (
    f[?] i ~@ f[+] .= {\i} => "{i}"
    f(i)
  )]
)
apply(fs[:], 0, 4) #= ["0", "one", "two", 3"]
fs[_:] #= [{1}, {2}]
```

### `[_:]` 

Cloned table of all parameter patterns  

```quarrel

```

#### Examples

```quarrel

```

### `[?]`

Check for a matching parameter pattern

```quarrel
{Container{Sequence}? self, Pattern? x} => Truth? result
```

#### Examples

```quarrel

```

### `[-]`

Change a routine by looking up it's parameter pattern

```quarrel
{Container{Sequence}? self, Sequence? x} => self
{Container{Sequence}? self, Routine? x} => self
```

#### Examples

```quarrel

```

### `[@]`

Count of how many routines are contained 

```quarrel
{Container{Sequence}? self} => Number? n
```

#### Examples

```quarrel

```

## Contract Graph

<ContractGraph id="sequence-graph">
graph LR;
  Routine --> Sequence;
  Sequence --> Aggregate;
  Aggregate --> Data;
  Data --> ac([Actor]);
  Sequence --> an([Any]);
</ContractGraph>