---
sidebar_position: 5
---
import ContractGraph from '@theme/ContractGraph';

# Tables

Tables are the only compound data structure available in Quarrel. They are [associative arrays](https://en.wikipedia.org/wiki/Associative_array) which, in Quarrel, means they store a collection of containers which can be looked up by means of the two available syntaxes: `\` tight infix operator and the `[...]` postcircumfix operator. Additionally, one can enter the context of a table by supplying a table container to a `->` operator (the applicand now has access to the internal contextual containers (keys)).

## How to...


### create a table

Tables are constructed using the `[]` circumfix operator. 

```quarrel
t .= []
```

Values can be added to the table literal, separated by `,` operators.

```quarrel
toppings .= ["pepperoni", "sausage", "artichokes", "black olives"]
```

Without container names for these literals, they are stored in the default positional containers 1, 2, 3, etc.

```quarrel
toppings[1] #= sausage
```

To supply a contextual container names, use the `.` and `:` postfix operators

```quarrel
toppings .= [pepperoni. 12, sausage. 5]
```

### convert to a 

```quarrel
ids .= <12, 31, 50>
idList .= [ids...]
```

### get a list of all of the named elements

```quarrel
channels .= [three. 3,seven. 7,'twenty-four'. 24]
chKeys .= channels[_:]
chSet .= <chKeys...> #= <[3, 7, 24]>
```

## Operators

Binary and unary operations that act specifically on Tables.

### `<:`

Appends a table to anther table (order matters)

#### Examples 

```quarrel
[1,2] <: 0 #= [[1,2], 0]
0 <: [1,2] #= [0, [1,2]]
```

### `>:`

To append but explode the outermost list element into the enclosing context (order matters)

#### Examples 

```quarrel
[1,2] >: 0 #= [1,2,0]
0 >: [1,2] #= [0,1,2]
```

### Method Operators

Many common ways to interact with lists are implemented by syntax within the postcircumfix square brackets. You can get the index of a value by using `?` as your index to get a check routine It returns either the index number or an empty sequence.
```quarrel
fruit .= ["apples", "oranges", "pears"]
|-| <- fruit[1] #= oranges
|-| <- fruit[?]("oranges") #= 1
|-| <- fruit[?] "bananas" #= ()
```

Add a new list items to the end by using the `[+]` postcirmfux method operator.

```quarrel
nums .= [10, 20, 30, 40]
nums[+] .= 50 #= [10, 20, 30, 40, 50]
timesTen .= => \1 * 10
nums[+] .= timesTen 6 #= [10, 20, 30, 40, 50, 60]
seventy .= 7
nums[+] .= seventy #= [10, 20, 30, 40, 50, 60, 7]
seventy .= seventy * 10
|-| <- nums #= [10, 20, 30, 40, 50, 60, 70]
```

Remove list items with `!`, takes one argument: the index to remove.

```quarrel
nums[!](0)
|-| <- nums #= [20, 30, 40, 50, 60, 70]
nums[!] 5
|-| <- nums #= [20, 30, 40, 50, 60]
nums[!] nums[@] - 1
|-| <- nums #= [20, 30, 40, 50]
```

First and last elements:
```quarrel
first .= l[<:]
last  .= l[:>]

```

To take the first or list while removing from the original list, use the `<!` or `!>` operators.
```quarrel
first .= l[<!] # shift (take first element)
last  .= l[!>] # pop (take last element)
```

Specify how many to shift or pop by placing a number next to the colon
```quarrel
first_two .= l[<:2]
last_two .= l[2:>]

```

To get a length of a list, use a @ inside brackets:
```quarrel
l .= [10,20,30]
len .= l[@] #= 3
```

To delete elements of a list, use `[!]` and provide the position to delete as a routine argument:
```quarrel
ls .= [1,2,"s",3]
ls[!](2) #= [1,2,3]
```

To set a value to empty, assign it `()!`:
```quarrel
l[1] .= ()!
```

### `-`

Resolves to all of the elements of the `x` Table that aren't in the `y` Table. Uses a `==` comparison between each element of the two tables.

```quarrel
{Table? x, Table? y} => Table? (...)
```

#### Examples

```quarrel
[1,2,3] - [1,2] #= [3]
[1,2,3] - [3,1,2] #= ()
["a", "b", "c"] - ["c"] #= ["a", "b"]
```

### `|`

Resolves to a union of all the elements of the `x` Table plus any *unique* elements of the `y` Table that aren't in the `x` Table.

```quarrel
{Table? x, Table? y} => Table? (...)
```

#### Examples

```quarrel
[1,2,3] | [1,2] #= [1,2,3]
[1,2,3] | [1,2,3,4] #= [1,2,3,4]
["a", "b", "c"] | ["c", "d", "e"] #= ["a", "b", "c", "d", "e"]
```

### `&`

Resolves to only the elements of the `x` and `y` Tables that are in both Tables.

```quarrel
{Table? x, Table? y} => Table? (...)
```

#### Examples

```quarrel
[1,2,3] & [1,2] #= [1,2]
[1,2,3] & [2,3,4] #= [2,3]
["a", "b", "c"] & ["c", "d", "e"] #= ["c"]
```

### `==`

Resolves to `true` if the left Table's potional values are the same as the right Table's positional values

```quarrel
{Table? x, Table? y} => Truth? (...)
```

#### Examples

```quarrel
[1,2,3] == [1,2] #= false
[1,2,3] == [1,2,3] #= true
[1, 2, foo. "bar"] == [1, 2, foo. "baz"] #= true
```

### `<>`

Resolves to `true` if the left Table potional values are **not** the same as the right Table's positional values

```quarrel
{Table? x, Table? y} => Truth? (...)
```

#### Examples

```quarrel
[1,2,3] <> [1,2] #= true
[1,2,3] <> [1,2,3] #= false
[1, 2, foo. "bar"] <> [1, 2, bar. "baz"] #= false
```

### `===`

Resolves to `true` if the left Table has the exact same positional and key-based values as the right Table

```quarrel
{Table? x, Table? y} => Truth? (...)
```

#### Examples

```quarrel
[1,2,3] === [1,2] #= true
[1,2,3] === [1,2,3] #= false
[1, 2, foo. "bar"] === [1, 2, foo. "baz"] #= false
```


### `>>`

Resolves to `true` if the left Table positional values are a strict superset of the right Table's positional values

```quarrel
{Table? x, Table? y} => Truth? (...)
```

#### Examples

```quarrel
[1,2,3] >> [1,2] #= true
[3,4,5] >> [3,7] #= false
```

### `<<`

Resolves to `true` if the left Table's positional values are a strict subset of the right Table's positional values

```quarrel
{Table? x, Table? y} => Truth? (...)
```

### `>=`

Resolves to `true` if the left Table's positional values are a superset of or equal to the right Table's positional values

```quarrel
{Table? x, Table? y} => Truth? (...)
```

#### Examples

```quarrel
[1,2,3] >= [1,2] #= true
[3,4,5] >= [3,4,5] #= true
[3,4,5] >= [3,4,5,7] #= false
```

### `<=`

Resolves to `true` if the left Table's positional valus are a subset of or equal to the right Table's positional values.

```quarrel
{Table? x, Table? y} => Truth? (...)
```

#### Examples

```quarrel
[1,2,3] <= [1,2,3] #= true
[3,4,5] <= [3,7,4,5] #= true
[3,4,5,2] <= [3,7,4,5] #= false
```

### `***`

Picks a number of random elements from the table. If the integer is greater than 1, returns a table of picked elements instead of just one.

```quarrel
{Table{\1}? x, 1}                   => \1? (...)
{1, Table? y}                       => \1? (...)
{Table{\1}? x, Integer? y @ _ >> 1} => Table{\1}? (...)
{Integer? x @ _ >> 1, Table{\1}? y} => Table{\1}? (...)
```

#### Examples

```quarrel
[1,2,3] *** 2 #= [3,1]
s .= ["apples","carrots","potatoes"]
s *** 1 #= "carrots"
s *** 2 #= ["potatoes", "carrots"]
s *** 1 #= "potatoes"
s *** s[@] #= ["carrots", "apples", "potatoes"] # shuffle
```



## Method Operators

Method operations that are performed on Table containers.

### `[x]`

Lookup the value corresponding to the key `x`

```quarrel
{Table? self, Datum? x} => Datum? (...)
```

#### Examples

```quarrel
colors .= ["Green", "Yellow", "Red"]
colors[2] #= "Red"
colors["Blue"] .= 10
colors["Blue"] #= 10
colors[3] #
```

### `[+]`

Add a value to a table at the next available numerical position

```quarrel
{Table? self, Datum? x} => self
```

#### Examples

```quarrel
tens .= [10, 20, 30]
tens[+] 40 #= [10, 20, 30, 40]
```

### `[@]` 

Return the length of the table

```quarrel
{Table? self} => Integer? (...)
```

#### Examples

```quarrel
fives .= <5, 10, 15>
fives[@] == 3 #= true
```

### `[\]`

Check if the table contains a certain key

```quarrel
{Table? self, {Text | Number}? x} => Truth? (...)
```

#### Examples

```quarrel
fruit .= ["apples", "oranges", "pears"]
fruit[\] 1 #= yes
fruit[\] 3 #= no
fruit[\] "value" #= no
fruit\value .= 154.96
fruit[\] "value" #= yes
```

### `[/]`

Check if the table contains a certain value, returning the location or `()`

```quarrel
{Table? self, {Text | Number}? x} => {Text | Number | ()}? (...)
```

#### Examples

```quarrel
fruit .= ["apples", "oranges", "pears"]
fruit[/]("oranges") #= 1
fruit[/] "bananas" #= no
fruit[/] 154.96 #= no
fruit\value = 154.96
fruit[/] 154.96 #= "value"
```


### `[!]`

Delete a value in a table with a key 

```quarrel
{Table? self, {Text | Number}? x} => self
```

#### Examples

```quarrel
ones .= [1,2,3,4.5,5]
ones[!](3) #= [1,2,3,5]
ones[/] 4.5 #= false
```

## Contract Graph

<ContractGraph id="table-graph">
graph LR;
  Table --> Aggregate;
  Aggregate --> Data;
  Data --> ac([Actor]);
  Table --> an([Any]);
</ContractGraph>