---
sidebar_position: 3
---

# Contributing

Due to my sporadic and chaotic spurts of activity in this project, it would be insincere to enlist additional involvement at this time. That being said, if you notice any issues with the code or are interested in understanding/debating my design decisions, I am open to a friendly dialogue via Gitlab issues. 

Outside of my cadence, I am also engaged in this project as a mental exercise and hobby. By that, I mean that I am enjoying the development of this project and would not wish to remove any burden of that process from myself.
