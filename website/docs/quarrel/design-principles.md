---
sidebar_position: 2
---

# Design Principles

What were the driving pillars of Quarrel's syntax and semantics?

## Anti-Angloanthropocentrism

Programming is an English language club.

## Common Keyboard Symbols Only

No unicode and no letters.

## Multi-Paradigm

Has to be flexible to different developers and problems

## Operators & Flows

Focus on providing operators and flows first-and-foremost--especially because symbols look more intuitive as connective operators (unary, binary, ternary).

## Concurrency-First

Provide truly "native" concurrency primitives
