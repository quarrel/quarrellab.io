---
sidebar_position: 1
---

# Introduction

Let's discover the basic "What", "how", and "why" of **Quarrel**.

## What is Quarrel?

Quarrel is a general-purpose programming language with dynamic typing, rich pattern matching, unique dataflow primitives and a syntax that has some inspiration from the depths of programming past but also carves many new paths. Quarrel makes little effort to be familiar, instead trying to break the mold that has shackled language design to the stuffy forms of Algol and Pascal.

## Status

Quarrel is still in early development. The syntax choices have mostly been ironed out. The documentation is currently being implemented as the specification for Quarrel. The grammar and ANTLR parser is also nearing a minimum viable "phase one" status. Work will soon begin on adding actual execution logic. I am building the parser in the standard established for the Truffle framework on the GraalVM. I find Truffle to be a fun and time-effective framework for language-building and one that provides the means to gain strong performance characteristics. At a later time, we may explore bootstrapping or developing an LLVM compiler.

## Vision

Quarrel makes every effort to remove the dependency on the English language. There are no operators or "keywords" that are word-based. All language syntax is handled by some combination of symbols that are common on many keyboard layouts from across the world. This removes often archaic or confusing keywords like `defn`, `hash`, `case`, `struct`, `null` and many more that even English speakers learning programming will encounter and have to shove into their mental toolkit. Quarrel seeks to both remove this barrier and provide a curated, intuitive set of symbolic operators that can express any program in a myriad of paradigmatic ways.

Beyond that, for the things that are best left named, there will be a system, tool and manual workflow that allows for easy i8n directly on your code! IDE and Code Editors will have extensions/plugins that do the part of replacing named things in the language of your choice.